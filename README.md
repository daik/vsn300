# ABB VSN-300 Weewx extension #

Using [weeWX](http://weewx.com/ "WeeWX - Open source software for your weather station") to record solar PV power generation data from an ABB VSN-300 enabled inverter and optionally publish this data to [PVOutput](http://pvoutput.org/ "PVOutput.org")

## Description ##

The *VSN300* extension allows weeWX to download, record and report solar PV power generation data from an ABB inverter equipped with a VSN-300 data logger card.. The *VSN300* extension also allows optional posting of solar PV power generation data to PVOutput using the PVOutput API.

The *VSN300* extension consists of:
- a weeWX driver for ABB VSN-300 equipped inverters,
- a custom weeWX database schema to support the ABB inverter,
- a RESTful service for posting data to PVOutput, and
- a utility for bulk uploading of solar PV generation data to PVOutput should Internet access be lost for some period of time.

**Note:** The weeWX driver used in the *VSN300* extension was developed using the *VSN-300* card and tested on an ABB PVI-5000-OUTD-AU, running firmware version XXX.

## Credits ##
This extension is built heavily on the work done by Gary Roderick and his [Aurora extension](https://github.com/gjr80/weewx-aurora/ "Aurora extension"). I am very grateful for the work Gary did and for allowing me (by choice of license and via e-mail) to build on his code.

## Pre-Requisites ##

The *VSN300* extension requires weeWX v3.7.0 or greater. Use of the *VSN300* extension to upload data to PVOutput requires a PVOutput account with system ID and API access key.

The *VSN300* extension requires a functioning TCP/IP communications link between the weeWX machine and the inverter. In my case, this was achieved using a Raspberry Pi 1 model B+ running Raspbian Jessie connected to a network that has a Wireless access point that the VSN-300 card connects to. This link functioned without the need for installation of any additional software other than the *VSN300* extension.

## Installation ##

The *VSN300* extension can be installed manually or automatically using the *wee_extension* utility. The preferred method of installation is through the use of *wee_extension*.

**Note:** Symbolic names are used below to refer to some file location on the weeWX system. These symbolic names allow a common name to be used to refer to a directory that may be different from system to system. The following symbolic names are used below:

-   *$DOWNLOAD_ROOT*. The path to the directory containing the downloaded *VSN300* extension.
-   *$HTML_ROOT*. The path to the directory where weeWX generated reports are saved. This directory is normally set in the *[StdReport]* section of *weewx.conf*. Refer to [where to find things](http://weewx.com/docs/usersguide.htm#Where_to_find_things "where to find things") in the weeWX [User's Guide](http://weewx.com/docs/usersguide.htm "User's Guide to the weeWX Weather System") for further information.
-   *$BIN_ROOT*. The path to the directory where weeWX executables are located. This directory varies depending on weeWX installation method. Refer to [where to find things](http://weewx.com/docs/usersguide.htm#Where_to_find_things "where to find things") in the weeWX [User's Guide](http://weewx.com/docs/usersguide.htm "User's Guide to the weeWX Weather System") for further information.
-   *$SKIN_ROOT*. The path to the directory where weeWX skin folders are located This directory is normally set in the *[StdReport]* section of *weewx.conf*. Refer to [where to find things](http://weewx.com/docs/usersguide.htm#Where_to_find_things "where to find things") in the weeWX [User's Guide](http://weewx.com/docs/usersguide.htm "User's Guide to the weeWX Weather System") for further information.

### Installation using the wee_extension utility ###

1.  Download the latest *VSN300* extension from the *VSN300* extension [releases page](https://bitbucket.org/daik/vsn300/downloads/) into a directory accessible from the weeWX machine.
        
        wget -P $DOWNLOAD_ROOT https://XXXX/XXXX.tar.gz

    where $DOWNLOAD_ROOT is the path to the directory where the *VSN300* extension is to be downloaded.

1.  Stop weeWX:

        sudo /etc/init.d/weewx stop

    or

        sudo service weewx stop

1.  Install the *VSN300* extension downloaded at step 1 using the *wee_extension* utility:

        wee_extension --install=$DOWNLOAD_ROOT/VSN300-0.1.0.tar.gz

    This will result in output similar to the following:

        Request to install '/var/tmp/VSN300-0.1.0.tar.gz'
        Extracting from tar archive /var/tmp/VSN300-0.1.0.tar.gz
        Saving installer file to /home/weewx/bin/user/installer/VSN300
        Saved configuration dictionary. Backup copy at /home/weewx/weewx.conf.20170215124410
        Finished installing extension '/var/tmp/VSN300-0.1.0.tar.gz'

1.  Edit *weewx.conf*:

        vi weewx.conf
        
1.  Locate the *[VSN300]* section and change the settings to suit your configuration with particular attention to the correct setting of the host and password option: 

        [VSN300]
            model = ABB PVI-5000
            driver = user.vsn300
            [[FieldMap]]
                string1Voltage = getStr1V
                string1Current = getStr1C
                string1Power = getStr1P
                string2Voltage = getStr2V
                string2Current = getStr2C
                string2Power = getStr2P
                gridVoltage = getGridV
                gridCurrent = getGridC
                gridPower = getGridP
                gridFrequency = getFrequency
                inverterTemp = getInverterT
                boosterTemp = getBoosterT
                bulkVoltage = getBulkV
                isoResistance = getIsoR
                bulkmidVoltage = getBulkMidV
                bulkdcVoltage = getBulkDcV
                leakdcCurrent = getLeakDcC
                leakCurrent = getLeakC
                griddcVoltage = getGridDcV
                gridavgVoltage = getGridAvV
                gridnVoltage = getPeakP
                griddcFrequency = getGridDcFreq
                dayEnergy = getDayEnergy
is fieldmap used?

1.  If data is to be posted to PVOutput, locate the *[StdRESTful] [[PVOutput]]* section ensuring *enable* is set to *true* and *system_id* and *api_key* are set appropriately: 

        [StdRESTful]
            [[PVOutput]]
                enable = true
                system_id = replace_me
                api_key = replace_me

    **Note:** *enable* is set to *false* by default during the *VSN300* extension installation.

1. Add schema to binding
Find the [[DataBindings]] section and add schema = user.vsn300_schema.vsn300_schema to the binding that you plan to use for the *VSN300* driver.

        [DataBindings]
         [[wx_binding]]
		database = archive_sqlite
		table_name = archive
		manager = weewx.wxmanager.WXDaySummaryManager
		schema = user.vsn300_schema.vsn300_schema

1.  Save *weewx.conf*.

1.  Edit *$BIN_ROOT/user/extension.py*:

        $ vi extensions.py

1.  Add the following lines to *extensions.py*:

        # ============================================================================
        #                  VSN300 units definitions and functions
        # ============================================================================
        
        import weewx.units
        
        # create groups for frequency and resistance
        weewx.units.USUnits['group_frequency'] = 'hertz'
        weewx.units.MetricUnits['group_frequency'] = 'hertz'
        weewx.units.MetricWXUnits['group_frequency'] = 'hertz'
        weewx.units.USUnits['group_resistance'] = 'ohm'
        weewx.units.MetricUnits['group_resistance'] = 'ohm'
        weewx.units.MetricWXUnits['group_resistance'] = 'ohm'


        # set default formats and labels for frequency and resistance
        weewx.units.default_unit_format_dict['hertz'] = '%.1f'
        weewx.units.default_unit_label_dict['hertz'] = ' ohm'
        weewx.units.default_unit_format_dict['ohm'] = '%.1f'
        weewx.units.default_unit_label_dict['ohm'] = ' ohm'
        
        # define conversion functions for resistance
        weewx.units.conversionDict['ohm'] = {'kohm': lambda x : x / 1000.0,
                                             'Mohm': lambda x : x / 1000000.0}
        weewx.units.conversionDict['kohm'] = {'ohm': lambda x : x * 1000.0,
                                              'Mohm': lambda x : x / 1000.0}
        weewx.units.conversionDict['Mohm'] = {'ohm': lambda x : x * 1000000.0,
                                              'kohm': lambda x : x * 1000.0}

        #The VSN-300 card is a not in sync with how weewx wants units, so we add a few specifically to convert from VSN300 format to weewx format

        weewx.units.conversionDict['MOhm'] = {'ohm': lambda x : x * 1000000.0}
        weewx.units.conversionDict['A'] = {'amp': lambda x : x}
        weewx.units.conversionDict['V'] = {'volt': lambda x : x}
        weewx.units.conversionDict['kWh'] = {'watt_hour': lambda x : x * 1000.0}
        weewx.units.conversionDict['HZ'] = {'hertz': lambda x : x}
        weewx.units.conversionDict['degC'] = {'degrees_c': lambda x : x}
        weewx.units.conversionDict['uA'] = {'amp': lambda x : x / 1000000}

        # assign database fields to groups
        weewx.units.obs_group_dict['string1Voltage'] = 'group_volt'
        weewx.units.obs_group_dict['string1Current'] = 'group_amp'
        weewx.units.obs_group_dict['string1Power'] = 'group_power'
        weewx.units.obs_group_dict['string2Voltage'] = 'group_volt'
        weewx.units.obs_group_dict['string2Current'] = 'group_amp'
        weewx.units.obs_group_dict['string2Power'] = 'group_power'
        weewx.units.obs_group_dict['gridVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['gridCurrent'] = 'group_amp'
        weewx.units.obs_group_dict['gridPower'] = 'group_power'
        weewx.units.obs_group_dict['gridFrequency'] = 'group_frequency'
        weewx.units.obs_group_dict['efficiency'] = 'group_percent'
        weewx.units.obs_group_dict['inverterTemp'] = 'group_temperature'
        weewx.units.obs_group_dict['boosterTemp'] = 'group_temperature'
        weewx.units.obs_group_dict['bulkVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['isoResistance'] = 'group_resistance'
        weewx.units.obs_group_dict['in1Power'] = 'group_power'
        weewx.units.obs_group_dict['in2Power'] = 'group_power'
        weewx.units.obs_group_dict['bulkmidVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['bulkdcVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['leakdcCurrent'] = 'group_amp'
        weewx.units.obs_group_dict['leakCurrent'] = 'group_amp'
        weewx.units.obs_group_dict['griddcVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['gridavgVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['gridnVoltage'] = 'group_volt'
        weewx.units.obs_group_dict['griddcFrequency'] = 'group_frequency'
        weewx.units.obs_group_dict['energy'] = 'group_energy'
        weewx.units.obs_group_dict['powerOut'] = 'group_power'
        weewx.units.obs_group_dict['dayEnergy'] = 'group_energy'

1.  Save *extensions.py*.
 
1.  Start weeWX:

        sudo /etc/init.d/weewx start

    or

        sudo service weewx start

The weeWX log should be monitored to verify data is being read from the inverter and, if posting to PVOutput is enabled, data is being posted to PVOutput at the end of each archive period. Setting *debug = 1* or *debug = 2* in *weewx.conf* will provide additional information in the log. Using *debug = 2* will generate significant amounts of log output and should only be used for verification of operation or testing.


## Support ##

General support issues may be raised in the Google Groups [weewx-user forum](https://groups.google.com/group/weewx-user "Google Groups weewx-user forum"). Specific bugs in the *VSN300* extension code should be the subject of a new issue raised via the *VSN300* extension [Issues Page](https://bitbucket.org/daik/vsn300/issues?status=new&status=open "VSN300 extension issues").
 
## Licensing ##

The *VSN300* extension is licensed under the [GNU Public License v3](https://bitbucket.org/daik/vsn300/src/7c030b9a685ea29b7d1c29b049e43086b60fa682/LICENSE?at=master "VSN300 extension license").
