#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
#                     Installer for VSN300 inverter weewx extension
#
# Version: 0.0.1                                        Date: 05 January 2018
#
# Revision History
#  05 January 2018    v0.0.1
#       - initial implementation
#
import weewx

from distutils.version import StrictVersion
from setup import ExtensionInstaller

REQUIRED_VERSION = "3.7.0"
VSN300_VERSION = "0.0.1"

def loader():
    return VSN300Installer()


class VSN300Installer(ExtensionInstaller):
    def __init__(self):
        print "hello"
        if StrictVersion(weewx.__version__) < StrictVersion(REQUIRED_VERSION):
            msg = "%s requires weeWX %s or greater, found %s" % ('VSN300 ' + VSN300_VERSION,
                                                                 REQUIRED_VERSION,
                                                                 weewx.__version__)
            raise weewx.UnsupportedFeature(msg)
        super(VSN300Installer, self).__init__(
                version=VSN300_VERSION,
                name='VSN300',
                description='WeeWX support for recording solar PV power generation data from a PVI-5000 ABB inverter.',
                author="Daniel Kjellin and Gary Roderick",
                author_email="info@daik.se",
                restful_services=[],
                config={
                    'VSN300': {
                        'model': 'replace_me',
                        'host': 'replace_me',
                        'driver': 'user.vsn300',
                    }
                },
                files=[
                    ('bin/user', ['bin/user/vsn300_schema.py']),
                    ('bin/user', ['bin/user/vsn300.py'])
                ]
            )

