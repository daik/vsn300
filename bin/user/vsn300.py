# vsn300_driver.py
#
# A weeWX driver for the ABB VSN-300 WiFi card.
#
# Copyright (C) 2018 Daniel Kjellin                info<at>daik.se
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see http://www.gnu.org/licenses/.
#
# Version: 0.1                                      Date: 12 January 2018
#
# Revision History
#   12 January 2018      v0.1    - initial release
#
""" A weeWX driver for the ABB VSN-300 WiFi card.

The driver communicates directly with the inverter without the need for any
other application. The driver produces loop packets that may be used with a
custom weeWX schema to produce archive records may be stored and processed by
the weeWX engine.
"""

import syslog

import time

import weewx.drivers

DRIVER_NAME = 'VSN300'
DRIVER_VERSION = '0.1'


def loader(config_dict, engine):  # @UnusedVariable
    return VSN300Driver(config_dict[DRIVER_NAME])

#def confeditor_loader():
#    return AuroraConfEditor()

# ============================================================================
#                            class VSN300Driver
# ============================================================================


class VSN300Driver(weewx.drivers.AbstractDevice):
    """Class representing connection to the VSN300 inverter."""

    def __init__(self, aurora_dict):
        """Initialise an object of type VSN300Driver."""
        # model
        self.model = aurora_dict.get('model', 'VSN300')
        logdbg('%s driver version is %s' % (self.model, DRIVER_VERSION))


    def genLoopPackets(self):
        """Generate packages to be ingested by Weewx.
            This function does not provide any way to
            "catch up" on old data, it will only return the most
            current data from the inverter.
        """
        counter = 0
        print "loop packets"
        while True:
            print "sleeping"
            time.sleep(30)

            yield {
                'time':int(time.time()),
                'data': counter
            }
            counter += 1

    def genStartupRecords(self, since_ts):
        """Generator function that returns records from the console.

        since_ts: local timestamp in seconds.  All data since (but not
                  including) this time will be returned.  A value of None
                  results in all data.

        yields: a sequence of dictionaries containing the data, each with
                local timestamp in seconds.
        """
        print "genStartupRecords"
        counter = 10
        while counter > 0:
            yield {
                'dateTime': int(time.time()- 60 * counter),
                'data': counter,
                'usUnits' : weewx.METRIC,
                #interval is the length of the archive in minutes
                'interval' : 10
            }
            counter -= 1
            print "GSR sleep then looping"
            time.sleep(1)

    def getTime(self):
        """Get inverter system time and return as an epoch timestamp.

        During startup weeWX uses the 'console' time if available. The way the
        driver tells weeWX the 'console' time is not available is by raising a
        NotImplementedError error when getTime is called. This is what is
        normally done for stations that do not keep track of time. In the case
        of the ABB inverter, when it is asleep we cannot get the time so in
        that case we return the system time, to indicate that we can return times

        Returns:
            An epoch timestamp representing the inverter date-time, or current console time if inverter is not awake.
        """
        #TODO: real-impl-needed
        return int(time.time())

    def setTime(self):
        """
        We can't set the time using a normal user account. Left the method in to make it explicit that it can not be done.
        :return:
        """
        raise NotImplementedError("Method 'setTime' not implemented")









def logmsg(level, msg):
    syslog.syslog(level, 'aurora: %s' % msg)


def logdbg(msg):
    logmsg(syslog.LOG_DEBUG, msg)


def logdbg2(msg):
    if weewx.debug >= 2:
        logmsg(syslog.LOG_DEBUG, msg)


def loginf(msg):
    logmsg(syslog.LOG_INFO, msg)


def logerr(msg):
    logmsg(syslog.LOG_ERR, msg)
