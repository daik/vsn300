#!/bin/bash
VERSION=0.1
FILE_NAME=vsn300-$VERSION.tar.gz
rm -f $FILE_NAME
rm -rf build
mkdir -p build/weewx-vsn300
cp -R bin skins changelog install.py LICENSE README.md weewx.conf build/weewx-vsn
cd build
tar -zcvf ../$FILE_NAME weewx-vsn
cd ..
